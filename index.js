const fs = require("fs");
const Jimp = require("jimp");
dir = ""; // insert directory with trailing \\
fs.readdir(dir, (err, files) => {
  for (const file of files)
    Jimp.read(dir + file, (err, lenna) => {
      if (err) throw err;
      lenna
        .resize(410, 512) // resize
        .quality(40) // set JPEG quality
        .write(dir + "resized\\" + file); // save
    });
});
